package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.ogm.datastore.document.options.AssociationStorage;
import org.hibernate.ogm.datastore.document.options.AssociationStorageType;

@Entity
@AssociationStorage(AssociationStorageType.ASSOCIATION_DOCUMENT)
public class Perro
{

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Version
    @Generated(value = GenerationTime.ALWAYS)
    @javax.persistence.Column(name = "_rev")
    private String revision;
    private String nombre;
    @OneToOne(cascade = javax.persistence.CascadeType.PERSIST)
    private Raza raza;

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String name)
    {
        this.nombre = name;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getRevision()
    {
        return revision;
    }

    public void setRevision(String revision)
    {
        this.revision = revision;
    }

    public Raza getRaza()
    {
        return raza;
    }

    public void setRaza(Raza breed)
    {
        this.raza = breed;
    }
}
