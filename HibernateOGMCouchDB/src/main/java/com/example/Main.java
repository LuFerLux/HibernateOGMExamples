package com.example;

import com.example.entity.Perro;
import com.example.entity.Raza;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.TransactionManager;
import org.hibernate.Transaction;
import org.hibernate.ogm.OgmSession;
import org.hibernate.ogm.util.impl.Log;
import org.hibernate.ogm.util.impl.LoggerFactory;

public class Main
{

    private static final Log logger = LoggerFactory.make();

    public static void main(String[] args)
    {
        TransactionManager tm = com.arjuna.ats.jta.TransactionManager.transactionManager();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("couchdb-hibernateogm");

        try
        {
            tm.begin();

            logger.infof("Guardar perro y raza");

            EntityManager em = emf.createEntityManager();
            OgmSession ogmSession = em.unwrap(OgmSession.class);

            Perro dina = new Perro();

            dina.setNombre("Nancy");

            Raza breed = new Raza();

            breed.setNombre("Ding");

            dina.setRaza(breed);

            Transaction tx = ogmSession.beginTransaction();

            ogmSession.save(dina);

            Long dinaId = dina.getId();

            tx.commit();

            ogmSession.close();

            logger.infof("obtener el perro y su raza");

            em = emf.createEntityManager();

            dina = em.find(Perro.class, dinaId);

            System.out.println("Perro: " + dina.getNombre() + " de raza: " + dina.getRaza().getNombre());

            em.flush();

            em.close();

            tm.commit();

            emf.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
