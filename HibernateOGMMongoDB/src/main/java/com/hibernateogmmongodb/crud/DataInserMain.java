/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hibernateogmmongodb.crud;

import com.hibernateogmmongodb.util.HibernateOGMUtil;
import com.hibernateogmmongodb.vo.Empleado;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author LuFerLux
 */
public class DataInserMain
{

    private static EntityManagerFactory entityManagerFactory;

    public static void main(String[] args)
    {
        entityManagerFactory = HibernateOGMUtil.getEntityManagerFactory();

        EntityManager em = entityManagerFactory.createEntityManager();

        Empleado employee = new Empleado();

        employee.setNombre("LuFerLux");

        em.getTransaction().begin();

        em.persist(employee);

        em.getTransaction().commit();

        em.close();

        HibernateOGMUtil.closeEntityManagerFactory();
    }
}
