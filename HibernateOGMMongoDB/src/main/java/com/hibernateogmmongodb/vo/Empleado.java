/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hibernateogmmongodb.vo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

/**
 *
 * @author LuFerLux
 */
@Entity
public class Empleado
{

    @TableGenerator(
            name = "tg",
            table = "pk_table",
            pkColumnName = "value",
            valueColumnName = "nombre",
            pkColumnValue = "snuf",
            allocationSize = 10)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "tg")
    private long id;

    private String nombre;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String name)
    {
        this.nombre = name;
    }

    @Override
    public String toString()
    {
        return "Empleado [id=" + id + ", nombre=" + nombre + "]";
    }

}
