/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hibernateogmmongodb.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author LuFerLux
 */
public class HibernateOGMUtil
{

    private static EntityManagerFactory entityManagerFactory = null;

    static
    {
        try
        {
            entityManagerFactory = Persistence.createEntityManagerFactory("persistence_unit");
        }
        catch (Exception e)
        {
            System.err.println("Initial EntityManagerFactory creation failed." + e);
        }
    }

    public static EntityManagerFactory getEntityManagerFactory()
    {
        return entityManagerFactory;
    }

    public static void closeEntityManagerFactory()
    {
        entityManagerFactory.close();
    }
}
