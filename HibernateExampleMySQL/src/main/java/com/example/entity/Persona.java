package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Banu Prakash
 Persona entity class mapped to PERSON_TABLE table of DB using JPA
 */
@Entity
@Table(name="PERSON")
public class Persona {
	
	/**
	 * Primary key column
	 */
	@Id
	@Column(name="PER_ID")
	private int personId;
	
	
	private String name;


	public Persona() {
		super();
	}


	public Persona(int personId, String name) {
		super();
		this.personId = personId;
		this.name = name;
	}


	public int getPersonId() {
		return personId;
	}


	public void setPersonId(int personId) {
		this.personId = personId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	
}
