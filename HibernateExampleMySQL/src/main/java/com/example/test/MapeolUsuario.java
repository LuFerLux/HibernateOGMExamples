package com.example.test;

import java.util.Date;

import org.hibernate.Session;

import com.example.entity.Usuario;
import com.example.util.HibernateUtil;

/**
 * 
 * @author Banu Prakash
 * 
 *         Hibernate Client
 */
public class MapeolUsuario {

	public static void main(String[] args) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			Usuario user = new Usuario();

			user.setUserId(111);
			user.setUsername("Kim");
			user.setCreatedBy("Application");
			user.setCreatedDate(new Date());

			session.save(user);
			session.getTransaction().commit();
		} finally {
			HibernateUtil.shutdown();
		}
	}

}
