package com.example.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * 
 * @author Banu Prakash
 * 
 *         Utility class to create Hibernate Session Factory
 */
public class HibernateUtil {
	
	/*
	 * StandardServiceRegistry defines the main Hibernate ServiceRegistry,This registry is generally built using the org.hibernate.boot.registry.StandardServiceRegistryBuilder class
	 * 
	 * By default it holds most of the Services used by Hibernate.
	 * Some particular StandardServiceRegistry Services of note include:
	 * a) ConnectionProvider/MultiTenantConnectionProvider [The Service providing Hibernate with Connections as needed]
	 * b) JdbcServices [is an aggregator Service (a Service that aggregates other Services) exposing unified functionality around JDBC accessibility.]
	 * c) TransactionFactory [how to control or integrate with transactions]
	 * d) JtaPlatform [When using a JTA-based TransactionFactory, the org.hibernate.engine.transaction.jta.platform.spi.JtaPlatform Service provides Hibernate access to the JTA TransactionManager and UserTransaction, as well handling Synchronization registration]
	 * 
	 */
	private static SessionFactory sessionFactory = buildSessionFactory();
	private static ServiceRegistry serviceRegistry;

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration configuration = new Configuration();
			configuration.configure();
			serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			sessionFactory = configuration.buildSessionFactory(
					serviceRegistry);
			return sessionFactory;
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}
